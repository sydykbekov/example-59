import React, {Component} from 'react';

class AddTaskForm extends Component {
    render() {
        return (
            <form>
                <input placeholder="Your task ..." value={this.props.text} onChange={this.props.handleChange} id="task" type="text"/>
                <button onClick={this.props.handleClick} id="add">Add</button>
            </form>
        );
    }
}

export default AddTaskForm;