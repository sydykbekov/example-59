import React, {Component} from 'react';
import './App.css';
import AddTaskForm from './components/AddTaskForm';
import Task from './components/Task';

class App extends Component {
    state = {
        tasks: [
            {text: "Hello", id: 1},
            {text: 'World!', id: 2}
        ],
        text: ''
    };

    handleChange = (event) => {
        this.setState({text: event.target.value})
    };

    handleClick = (e) => {
        e.preventDefault();
        if (this.state.text !== '') {
            let obj = {
                text: this.state.text,
                id: Date.now()
            };
            const tasks = [...this.state.tasks];
            tasks.unshift(obj);
            this.setState({tasks, text: ''});
            console.log(this.state);
        } else {
            alert('Please add a task!');
        }
    };

    changePost = (event, index) => {
        let taskIndex;
        const tasks = [...this.state.tasks];
        tasks.forEach((item, i) => item.id === index ? taskIndex = i : null);

        const task = {...this.state.tasks[taskIndex]};
        task.text = event.target.value;

        tasks[taskIndex] = task;
        this.setState({tasks});
    };

    removeTask = (id) => {
        console.log(id);
        const index = this.state.tasks.findIndex(div => div.id === id);
        const tasks = [...this.state.tasks];
        tasks.splice(index, 1);
        this.setState({tasks});
    };

    render() {
        return (
            <div className="App">
                <AddTaskForm text={this.state.text} handleClick={this.handleClick} handleChange={this.handleChange}/>
                {this.state.tasks.map((task) =>
                    <Task key={task.id} change={(event) => this.changePost(event, task.id)} removeTask={() => this.removeTask(task.id)} text={task.text}/>
                )}
            </div>
        );
    }
}

export default App;